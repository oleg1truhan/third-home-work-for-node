const express = require('express');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 8080;
const authRouter = require('./routers/auth_router');
const userRouter = require('./routers/user_router');
const truckRouter = require('./routers/truck_router');
const loadRouter = require('./routers/load_router');
const app = express();

app.use(express.json());
app.use("/api", authRouter);
app.use('/api', userRouter);
app.use('/api', truckRouter);
app.use('/api', loadRouter);

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://Oleshka_der:qwerty123@cluster0.oytzn.mongodb.net/thirdHW');
        app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
    } catch (e) {
        console.log(e);
    }
}

start();
