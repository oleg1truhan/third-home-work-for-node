const {Schema, model} = require('mongoose');

const Truck = new Schema({
        created_by: {type: Schema.Types.ObjectId, ref: 'User'},
        assigned_to: {type: Schema.Types.ObjectId, ref: 'User'},
        type: {type: String, enum: [ 'SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT' ], required: true},
        status: {type: String, required: true, enum: [ 'OL', 'IS' ], default: 'IS'},
        created_date: {type: Date, default: Date.now()},
    }, {versionKey: false},
);

module.exports = model('Truck', Truck);
