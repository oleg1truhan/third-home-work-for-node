const Load = require('../models/Load');
const Truck = require('../models/Truck');

class LoadController {
    async getLoads(req, res) {
        try {
            const {roles} = req.user;
            const loads = await Load.find();
            const shipperListLoads = loads.filter(el => el.status === 'NEW' || el.status === 'POSTED' || el.status === 'ASSIGNED');
            if(roles.includes('DRIVER')) {
                await res.status(200).json({loads: loads});
            } else {
                await res.status(200).json({loads: shipperListLoads});
            }
        } catch (err) {
            await res.status(500).json({message: err});
        }
    }

    async addLoad(req, res) {
        try {
            const {id} = req.user;
            const load = await Load.create({
                created_by: id,
                status: req.body.status,
                name: req.body.name,
                payload: req.body.payload,
                pickup_address: req.body.pickup_address,
                delivery_address: req.body.delivery_address,
                dimensions: {
                    width: req.body.dimensions.width,
                    length: req.body.dimensions.length,
                    height: req.body.dimensions.height
                },
            });
            await load.save();
            if (!load) {
                return res.status(400).json({
                    message: 'Please, specify content',
                });
            }
            await res.json({
                message: 'Load created successfully'
            });
            console.log(load)
        } catch (err) {
            console.log(err);
            await res.status(500).json(err);
        }
    }

    async getLoadActive(req, res) {
        try {
            const loads = await Load.find();
            const activeListLoads = loads.filter(el => el.status === 'NEW' || el.status === 'POSTED' || el.status === 'ASSIGNED');
            if (!activeListLoads) {
                return res.status(400).json({message: 'Don`t find active load'});
            }
            await res.status(200).json({load: activeListLoads});
        }catch (err) {
            return res.status(500).json({message: 'Server Error'});
        }
    }

    async patchLoad(req, res) {
        try {
            const thirdPatch = await Load.findOneAndUpdate({state: 'En route to delivery'}, {$set: {state: 'Arrived to delivery'}},)

            const secondPatch = await Load.findOneAndUpdate({state: 'Arrived to Pick Up'}, {$set: {state: 'En route to delivery'}},)

            const firstPatch = await Load.findOneAndUpdate({state: 'En route to Pick Up'}, {$set: {state: 'Arrived to Pick Up'}},)

            if (firstPatch) {
                console.log(firstPatch.state);
                return res.status(200).send({
                    message: 'Load state changed to Arrived to Pick Up'
                });
            }
            if (secondPatch) {
                console.log(secondPatch.state);
                return res.status(200).send({
                    message: 'Load state changed to En route to delivery'
                });
            }
            if (thirdPatch) {
                console.log(thirdPatch.state);
                return res.status(200).send({
                    message: 'Load state changed to Arrived to delivery'
                });
            }
        } catch (err) {
            console.log(err);
            await res.status(500).json({message: err});
        }
    }

    async getLoad(req, res) {
        Load.findById(req.params.id, (err, load) => {
            if (err) {
                console.log({message: 'Server Error'})
                return res.status(500).json({message: 'Server Error'});
            }
            if (!load) {
                console.log({message: 'Don`t find load'})
                return res.status(400).json({message: 'Don`t find load'});
            }else {
                console.log({load: load});
                return res.status(200).json({load: load});
            }
        });
    }

    async updateLoad(req, res) {
        try {
            const putLoad = await Load.findOneAndUpdate(
                {_id: req.params.id}, {$set: {
                        name: req.body.name,
                        payload: req.body.payload,
                        pickup_address: req.body.pickup_address,
                        delivery_address: req.body.delivery_address,
                        dimension: {
                            width: req.body.dimensions.width,
                            length: req.body.dimensions.length,
                            height: req.body.dimensions.height
                        }
                    }},
            );
            if (!putLoad) {
                return res.status(400).json({
                    message: `Not found load with ID:${req.params.id}`,
                });
            }
            await res.status(200).json({
                message: 'Load details changed successfully'
            });
            console.log({putTruck: putLoad});
        } catch (err) {
            err = 'Server Error';
            console.log(err);
            await res.status(500).json({message: err});
        }
    }

    async deleteLoad(req, res) {
        await Load.deleteOne({_id: req.params.id});
        await res.status(200).json({message: 'Load deleted successfully'});
        console.log({message: 'Load deleted successfully'});
    }

    async postLoad(req, res) {
        try {
            const {id} = req.user;
            const load = await Load.findByIdAndUpdate(req.params.id,
                {$set: {
                        status: 'POSTED'}
                });
            const findTruck = await Truck.findOneAndUpdate({status : 'IS'},
                {$set: {
                        status: 'OL',
                        assigned_to: id}
                });
            await findTruck.save();
            if (!findTruck) {
                return res.status(400).json({
                    message: 'Driver not found',
                });
            }
            const loadChangeStatus = await Load.findOneAndUpdate({_id: req.params.id},
                {$set: {
                        assigned_to: findTruck._id,
                        status: 'ASSIGNED',
                        state: 'En route to Pick Up',
                        logs: {message: 'En route to Pick Up'}
                    }
                });
            if (!load) {
                return res.status(400).json({
                    message: 'Please, enter type',
                });
            }
            await res.status(200).json({
                message: "Load posted successfully",
                driver_found: true
            });
        } catch (err) {
            console.log(err);
            await res.status(500).json(err);
        }
    }

    async getLoadShippingInfo(req, res) {
        try {
            const load = await Load.findOne({ _id: req.params.id });
            if (!load) {
                return res.status(400).json({
                    message: 'Not found load',
                });
            }
            const truck = await Truck.findOne({_id: load.assigned_to});
            if (!truck) {
                return res.status(400).json({
                    message: 'Not found truck',
                });
            }
            await res.status(200).json({load, truck});
        } catch (err) {
            console.log(err);
            await res.status(500).json({message: err});
        }
    }
}

module.exports = new LoadController();
