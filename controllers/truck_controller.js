const Truck = require('../models/Truck');

class TruckController {
    async getTrucks(req, res) {
        try {
            const trucks = await Truck.find();
            await res.json({trucks: trucks});
            console.log(trucks);
        } catch (err) {
            await res.status(500).json({message: err});
        }
    }

    async addTruck(req, res) {
        try {
            const {id} = req.user;
            const truck = await Truck.create({
                created_by: id,
                type: req.body.type,
            });
            await truck.save();
            await res.json({
                message: "Truck created successfully"
            });
        } catch (err) {
            if (err.errors.type) {
                await res.status(400).json({
                    message: 'Error type',
                });
            } else {
                console.log(err);
                await res.status(500).json(err);
            }
        }
    }

    async getTruck(req, res) {
        Truck.findById(req.params.id, (err, truck) => {
            if (err) {
                return res.status(500).json({message: 'Server Error'});
            }
            if (!truck) {
                return res.status(400).json({message: 'Don`t find truck'});
            }else {
                return res.status(200).json({truck: truck});
            }
        });
    }

    async updateTruck(req, res) {
        try {
            const putTruck = await Truck.findOneAndUpdate(
                {_id: req.params.id}, {$set: {type: req.body.type}},
            );
            if (!putTruck) {
                return res.status(400).json({
                    message: `Not found truck with ID:${req.params.id}`,
                });
            }
            await res.status(200).json({
                message: 'Truck details changed successfully'
            });
            console.log({putTruck});
        } catch (err) {
            err = 'Server Error';
            console.log(err);
            await res.status(500).json({message: err});
        }
    }

    async deleteTruck(req, res) {
        await Truck.deleteOne({_id: req.params.id});
        res.status(200).send({message: 'Truck deleted successfully'});
        console.log({message: 'Truck deleted successfully'});
    }

    async assignTruck(req, res) {
        try {
            const {id} = req.user;
            if (await Truck.findOne({assigned_to: id})) {

                return res.status(400).json({
                    message: 'Driver can assign only one truck to himself',
                });
            }

            const truck = await Truck.findOne({_id: req.params.id});
            if (!truck) {
                return res.status(400).json({
                    message: 'Don`t find truck',
                });
            }
            if (truck.assigned_to) {
                return res.status(400).json({
                    message: 'Busy',
                });
            }
            truck.assigned_to = id;
            await truck.save();
            await res.json({
                message: "Truck assigned successfully"
            });
        } catch (err) {
            err = 'Server error';
            console.log(err);
            await res.status(500).json({message: err});
        }
    }
}

module.exports = new TruckController();
