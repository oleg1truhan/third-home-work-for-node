const User = require('../models/User');
const Role = require('../models/Role');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator');
const {secret} = require("../config");

const generateAccessToken = (id, email, password, roles) => {
    const payload = {
        id,
        email,
        password,
        roles
    }
    return jwt.sign(payload, secret, {expiresIn: "24h"} )
}

class AuthController {
    async registration(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({message: "Registration error", errors});
            }
            const {email, password} = req.body;
            const candidate = await User.findOne({email});
            if (candidate) {
                return res.status(400).json({message: "A user with the same name already exists"});
            }
            const hashPassword = bcrypt.hashSync(password, 7);
            const userRole = await Role.findOne({value: req.body.role});
            const user = new User({email, password: hashPassword, role: [userRole.value]});
            await user.save();
            return res.json({
                message: "Profile created successfully"
            });
        } catch (e) {
            console.log(e);
            await res.status(400).json({message: 'Registration error'});
        }
    }

    async login(req, res) {
        try {
            const {email, password} = req.body;
            const user = await User.findOne({email});
            if (!user) {
                return res.status(400).json({message: `User ${email} not found`});
            }
            const validPassword = bcrypt.compareSync(password, user.password);
            if (!validPassword) {
                return res.status(400).json({message: `Wrong password entered`});
            }
            const token = generateAccessToken(user._id, user.email, user.password, user.role);
            return res.json({jwt_token: token});
        } catch (e) {
            console.log(e);
            await res.status(400).json({message: 'Login error'});
        }
    }

    async getUsers(req, res) {
        try {
            const users = await User.find();
            await res.json(users);
        } catch (e) {
            console.log(e);
        }
    }

}

module.exports = new AuthController()
