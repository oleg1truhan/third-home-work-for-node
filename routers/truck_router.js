const {Router} = require('express');
const truckRouter = new Router();
const truckController = require('../controllers/truck_controller');
const authMiddleWare = require('../middlewaree/auth_middleware');
const roleMiddleware = require('../middlewaree/role_middleware');

truckRouter.get('/trucks', roleMiddleware(["DRIVER"]), truckController.getTrucks);

truckRouter.post('/trucks', authMiddleWare, roleMiddleware(["DRIVER"]), truckController.addTruck);

truckRouter.get('/trucks/:id', roleMiddleware(["DRIVER"]), truckController.getTruck);

truckRouter.put('/trucks/:id', roleMiddleware(["DRIVER"]), truckController.updateTruck);

truckRouter.delete('/trucks/:id', roleMiddleware(["DRIVER"]), truckController.deleteTruck);

truckRouter.post('/trucks/:id/assign', authMiddleWare, roleMiddleware(["DRIVER"]), truckController.assignTruck);

module.exports = truckRouter;
